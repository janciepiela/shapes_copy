#include <iostream>
#include <vector>
#include "shapes.h"

using namespace std;

int main()
{
  std::vector<Shape*> myShapes;
  myShapes.push_back(new Square(2));
  myShapes.push_back(new Rectangle(2,5));
  myShapes.push_back(new Circle(4));

  for (unsigned i = 0; i < myShapes.size(); ++i)
    std::cout << "Area of shape " << i << " is " << myShapes[i]-> area();
  
  for (Shape* s: myShapes)
    delete s;
  myShapes.clear();
		    
}
